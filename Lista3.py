import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# Lotka Volterra
def LV(t, x, y, a, b, c, d):
    one = (a - b * y) * x
    two = (c * x - d) * y
    return one, two

def LV_odeint(y, t, a, b, c, d):
    x, y = y
    dxdt = (a - b * y) * x
    dydt = (c * x - d) * y
    return [dxdt, dydt]

def Euler(a, b, c, d, x0, y0, t_start, t_end, dt):
    t_span = np.arange(t_start, t_end + dt, dt)
    x_LV_E = np.zeros_like(t_span)
    y_LV_E = np.zeros_like(t_span)
    x_LV_E[0] = x0
    y_LV_E[0] = y0

    for i in range(1, len(t_span)):
        t = t_span[i - 1]
        x = x_LV_E[i - 1]
        y = y_LV_E[i - 1]
        dxdt, dydt = LV(t, x, y, a, b, c, d)
        x_LV_E[i] = x + dt * dxdt
        y_LV_E[i] = y + dt * dydt

    return t_span, x_LV_E, y_LV_E

# Parametry
a = 1.2
b = 0.6
c = 0.3
d = 0.8

# Warunki początkowe - Euler
x0 = 2
y0 = 1
# Warunki początkowe - odeint
initial_conditions = [2, 1]

# Czas symulacji - Euler
t_start = 0.01
t_end = 25
dt = 0.01
# Czas symulacji - odeint
t = np.arange(0, 25, 0.01)

# Metoda Eulera
t_span, x_LV_E, y_LV_E = Euler(a, b, c, d, x0, y0, t_start, t_end, dt)
# Odeint
odeint_solution_LV = odeint(LV_odeint, initial_conditions, t, args=(a, b, c, d))

# # Wykres - Euler
# plt.figure(figsize=(12, 6))
# plt.plot(t_span, x_LV_E, label='Ofiary x(t)')
# plt.plot(t_span, y_LV_E, label='Drapieżniki y(t)')
# plt.xlabel('Czas')
# plt.ylabel('Populacja')
# plt.title('Lotka Volterra - Euler')
# plt.legend()
# plt.grid(True)

# # Wykres - odeint
# plt.figure(figsize=(12, 6))
# plt.plot(t, odeint_solution_LV[:, 0], label='Ofiary x(t)')
# plt.plot(t, odeint_solution_LV[:, 1], label='Drapieżniki y(t)')
# plt.xlabel("Czas")
# plt.ylabel("Populacja")
# plt.title("Lotka Volterra - odeint")
# plt.legend()
# plt.grid(True)

##########################################################################################################################################################
# Lorenz

def Lorenz_E(t, x, y, z, sigma, rho, beta):
    one = sigma * (y - x)
    two = x * (rho - z) - y
    three = x * y - beta * z
    return one, two, three

def Lorenz_odeint(y, t, sigma, rho, beta):
        x, y, z = y
        dxdt = sigma * (y - x)
        dydt = x * (rho - z) - y
        dzdt = x * y - beta * z
        return [dxdt, dydt, dzdt]

def Euler(sigma, rho, beta, x0, y0, z0, t_start, t_end, dt):
    t_span = np.arange(t_start, t_end + dt, dt)
    x_L_E = np.zeros_like(t_span)
    y_L_E = np.zeros_like(t_span)
    z_result_E = np.zeros_like(t_span)
    x_L_E[0] = x0
    y_L_E[0] = y0
    z_result_E[0] = z0

    for i in range(1, len(t_span)):
        t = t_span[i - 1]
        x = x_L_E[i - 1]
        y = y_L_E[i - 1]
        z = z_result_E[i - 1]
        dxdt, dydt, dzdt = Lorenz_E(t, x, y, z, sigma, rho, beta)
        x_L_E[i] = x + dt * dxdt
        y_L_E[i] = y + dt * dydt
        z_result_E[i] = z + dt * dzdt

    return t_span, x_L_E, y_L_E, z_result_E

# Parametry
sigma = 10
rho = 28
beta = 8 / 3

# Warunki początkowe - Euler
x0 = 1
y0 = 1
z0 = 1
# Warunki początkowe - odeint
initial_conditions = [1, 1, 1]

# Czas symulacji - Euler
t_start = 0.03 
t_end = 25
dt = 0.03
# Czas symulacji - odeint
t = np.arange(0, 25, 0.03)

# Metoda Eulera
t_span, x_L_E, y_L_E, z_result_E = Euler(sigma, rho, beta, x0, y0, z0, t_start, t_end, dt)
# odeint
odeint_solution_L = odeint(Lorenz_odeint, initial_conditions, t, args=(sigma, rho, beta))

# Wykresy - Euler
plt.figure(figsize=(10, 4))

plt.subplot(2, 3, 1)
plt.plot(x_L_E, y_L_E)
plt.xlabel('x')
plt.ylabel('y')
plt.title('Lorenz - Euler: y(x)')

plt.subplot(2, 3, 2)
plt.plot(x_L_E, z_result_E)
plt.xlabel('x')
plt.ylabel('z')
plt.title('Lorenz - Euler: z(x)')

plt.subplot(2, 3, 3)
plt.plot(y_L_E, z_result_E)
plt.xlabel('y')
plt.ylabel('z')
plt.title('Lorenz - Euler: z(y)')

# Wykres 3D
fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(111, projection='3d')
ax.plot(x_L_E, y_L_E, z_result_E, lw=0.5)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.set_title('Układ Lorenza')
plt.show()


# Wykresy - odeint
plt.figure(figsize=(10, 4))

plt.subplot(2, 3, 1)
plt.plot(odeint_solution_L[:, 0], odeint_solution_L[:, 1])
plt.xlabel('x')
plt.ylabel('y')
plt.title('Lorenz - odeint: y(x)')

plt.subplot(2, 3, 2)
plt.plot(odeint_solution_L[:, 0], odeint_solution_L[:, 2])
plt.xlabel('x')
plt.ylabel('z')
plt.title('Lorenz - odeint: z(x)')

plt.subplot(2, 3, 3)
plt.plot(odeint_solution_L[:, 1], odeint_solution_L[:, 2])
plt.xlabel('y')
plt.ylabel('z')
plt.title('Lorenz - odeint: z(y)')

# Wykres 3D
fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(111, projection='3d')
ax.plot(odeint_solution_L[:, 0], odeint_solution_L[:, 1], odeint_solution_L[:, 2], lw=0.5)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.set_title('Układ Lorenza')

#####################################################################################################################

def Blad_Aproksymacyjny(euler, odeint):
    error = np.abs(euler - odeint)
    return np.mean(error)

# BA LV
eulerLV_x, eulerLV_y = x_LV_E, y_LV_E
odeintLV_x, odeintLV_y = odeint_solution_LV[:, 0], odeint_solution_LV[:, 1]
ba_lv = (Blad_Aproksymacyjny(eulerLV_x, odeintLV_x) + Blad_Aproksymacyjny(eulerLV_y, odeintLV_y))/2
print("BA LV:", ba_lv)

# BA Lorenz
eulerL_x, eulerL_y, eulerL_z = x_L_E, y_L_E, z_result_E
odeintL_x, odeintL_y, odeintL_z = odeint_solution_L[:, 0], odeint_solution_L[:, 1], odeint_solution_L[:, 2]
ba_l = (Blad_Aproksymacyjny(eulerL_x, odeintL_x)+Blad_Aproksymacyjny(eulerL_y, odeintL_y)+Blad_Aproksymacyjny(eulerL_z, odeintL_z))/3
print("BA L:", ba_l)